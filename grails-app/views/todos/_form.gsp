<%@ page import="todo.Todos" %>



<div class="fieldcontain ${hasErrors(bean: todosInstance, field: 'activity', 'error')} required">
	<label for="activity">
		<g:message code="todos.activity.label" default="Activity" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="activity" required="" value="${todosInstance?.activity}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: todosInstance, field: 'priority', 'error')} required">
	<label for="priority">
		<g:message code="todos.priority.label" default="Priority" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="priority" required="" value="${todosInstance?.priority}"/>

</div>

