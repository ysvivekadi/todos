<%@ page import="todo.Todos" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
  <link rel="stylesheet" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
  <script type="text/javascript" src="http://t4t5.github.io/sweetalert/dist/sweetalert-dev.js"></script>
</head>
<body>  
	<div class="container-fluid">
	<h1><p class="text-center">Todos List</p></h1>
		<ul id="todos" class="list-group">
			<g:each var="todo" in="${todos}">
			  	<button type="button" class="list-group-item" onClick="edittodo('${todo.id}')">
			  		<span class="badge">${todo.priority}</span>
					${todo.activity}  
			  	</button> 
			</g:each>
		</ul>
	</div>
	<div class="container">
		<p class="text-center"><a href="create" role="button"><button type="button" class="btn btn-success">Create</button></a></p>		
	</div>
</body>
<script type="text/javascript">
	function edittodo(item) { 
		window.location = "/todo/todos/edit/" + item ;			  	 
	}	 
</script>
</html>
 