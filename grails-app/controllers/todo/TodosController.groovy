package todo



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TodosController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        def todos = Todos.list(max:10)
        render(view: "index", model: [todos: todos])
        return 
    }

    def show(Todos todosInstance) {
        respond todosInstance
    }

    def create() {
        respond new Todos(params)
    }

    @Transactional
    def save(Todos todosInstance) {
        if (todosInstance == null) {
            notFound()
            return
        }

        if (todosInstance.hasErrors()) {
            respond todosInstance.errors, view:'create'
            return
        }

        todosInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'todos.label', default: 'Todos'), todosInstance.id])
                redirect todosInstance
            }
            '*' { respond todosInstance, [status: CREATED] }
        }
    }

    def edit(Todos todosInstance) {
        respond todosInstance
    }

    @Transactional
    def update(Todos todosInstance) {
        if (todosInstance == null) {
            notFound()
            return
        }

        if (todosInstance.hasErrors()) {
            respond todosInstance.errors, view:'edit'
            return
        }

        todosInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Todos.label', default: 'Todos'), todosInstance.id])
                redirect todosInstance
            }
            '*'{ respond todosInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Todos todosInstance) {

        if (todosInstance == null) {
            notFound()
            return
        }

        todosInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Todos.label', default: 'Todos'), todosInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'todos.label', default: 'Todos'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
